<?php

// Defines
define( 'FL_CHILD_THEME_DIR', get_stylesheet_directory() );
define( 'FL_CHILD_THEME_URL', get_stylesheet_directory_uri() );

remove_action('wp_head', 'wp_generator');
// Classes
require_once 'classes/class-fl-child-theme.php';

// Actions
add_action( 'wp_enqueue_scripts', 'FLChildTheme::enqueue_scripts', 1000 );

add_action( 'wp_enqueue_scripts', function(){
    wp_enqueue_script("slick",get_stylesheet_directory_uri()."/resources/slick/slick.min.js","","",1);
    wp_enqueue_script("cookie",get_stylesheet_directory_uri()."/resources/jquery.cookie.min.js","","",1);
   wp_enqueue_script("child-script",get_stylesheet_directory_uri()."/script.js","","",1);
});


// Register menus
function register_my_menus() {
    register_nav_menus(
        array(
            'footer-1' => __( 'Footer Menu 1' ),
            'footer-2' => __( 'Footer Menu 2' ),
            'footer-3' => __( 'Footer Menu 3' ),
            'footer-4' => __( 'Footer Menu 4' ),
            'footer-5' => __( 'Footer Menu 5' ),
            'site-map' => __( 'Site Map' ),
        )
    );
}
add_action( 'init', 'register_my_menus' );


 
// Enable shortcodes in text widgets
add_filter('widget_text','do_shortcode');






// Move Yoast to bottom
function yoasttobottom()
{
    return 'low';
}

add_filter('wpseo_metabox_prio', 'yoasttobottom');





function fr_img($id=0,$size="",$url=false,$attr=""){

    //Show a theme image
    if(!is_numeric($id) && is_string($id)){
        $img=get_stylesheet_directory_uri()."/images/".$id;
        if(file_exists(to_path($img))){
            if($url){
                return $img;
            }
            return '<img src="'.$img.'" '.($attr?build_attr($attr):"").'>';
        }
    }

    //If ID is empty get the current post attachment id
    if(!$id){
        $id=get_post_thumbnail_id();
    }

    //If Id is object it means that is a post object, thus retrive the post ID
    if(is_object($id)){
        if(!empty($id->ID)){
            $id=$id->ID;
        }
    }

    //If ID is not an attachment than get the attachment from that post
    if(get_post_type($id)!="attachment"){
        $id=get_post_thumbnail_id($id);
    }

    if($id){
        $image_url=wp_get_attachment_image_url($id,$size);
        if(!$url){
            //If image is a SVG embed the contents so we can change the color dinamically
            if(substr($image_url,-4,4)==".svg"){
                $image_url=str_replace(get_bloginfo("url"),ABSPATH."/",$image_url);
                $data=file_get_contents($image_url);
                echo strstr($data,"<svg ");
            }else{
                return wp_get_attachment_image($id,$size,0,$attr);
            }
        }else if($url){
            return $image_url;
        }
    }
}

//Facet Title Hook
add_filter( 'facetwp_shortcode_html', function( $output, $atts ) {
    if ( isset( $atts['facet'] ) ) {       
        $output= '<div class="facet-wrap"><strong>'.$atts['title'].'</strong>'. $output .'</div>';
    }
    return $output;
}, 10, 2 );

add_filter('wp_nav_menu_items', 'do_shortcode');

/**
 * Dequeue the jQuery UI styles.
 *
 * Hooked to the wp_print_styles action, with a late priority (100),
 * so that it is after the style was enqueued.
 */
 function remove_pagelist_css() {
    wp_dequeue_style( 'page-list-style' );
 }
 add_action( 'wp_print_styles', 'remove_pagelist_css', 100 );

/* Gravity Form Time Field validation   */
add_filter( 'gform_field_validation', 'validate_time', 10, 4 );
function validate_time( $result, $value, $form, $field ) {

	if($field->type == 'time' ){
		//convert the entire time field array into a string, separating values with colons
		$input_time = implode( ':', $value );
		//replace colon between the time and am/pm with space and convert strings into a unix timestamp
		$time = strtotime( substr_replace( $input_time, ' ', -3, 1 ) );
		$max_time = strtotime( '17:00' );
		$min_time = strtotime( '09:00' );
		if ( $time < $min_time OR $time > $max_time ) {
			$result['is_valid'] = false;
			$result['message'] = 'Please select a time between 09:00 am and 04:00 pm';
		}
	}
    
    return $result;
}

/* Gravity Form same date diffrent time checking hook */
/* Shop at Home Form */
add_filter( 'gform_field_validation_35_17', function ( $result, $value, $form, $field ) {
        $dateOne = rgpost( 'input_13' );
        $dateTwo = rgpost( 'input_16' );
        $timeOne = rgpost( 'input_14' );
        if($dateOne == $dateTwo){
            if ( $result['is_valid'] && $value == $timeOne ) {
                $result['is_valid'] = false;
                $result['message']  = 'Please enter a different Time.';
            }
        }
    return $result;
}, 10, 4 );
add_filter( 'gform_field_validation_35_16', function ( $result, $value, $form, $field ) {
	
			     $dateOne = rgpost( 'input_13' );       
				if ( $result['is_valid'] && $value < $dateOne ) {
					$result['is_valid'] = false;
					$result['message']  = 'Second Requested Date Should Be Greater Than First Requested Date';
				}
			
    return $result;
}, 10, 4 );


/* Schedule Showroom Visit Form */
add_filter( 'gform_field_validation_39_17', function ( $result, $value, $form, $field ) {
        $dateOne = rgpost( 'input_13' );
        $dateTwo = rgpost( 'input_16' );
        $timeOne = rgpost( 'input_14' );
        if($dateOne == $dateTwo){
            if ( $result['is_valid'] && $value == $timeOne ) {
                $result['is_valid'] = false;
                $result['message']  = 'Please enter a different Time.';
            }
        } else if ($dateOne < $dateTwo){
			
		}
    return $result;
}, 10, 4 );

add_filter( 'gform_field_validation_39_16', function ( $result, $value, $form, $field ) {
	
			     $dateOne = rgpost( 'input_13' );       
				if ( $result['is_valid'] && $value < $dateOne ) {
					$result['is_valid'] = false;
					$result['message']  = 'Second Requested Date Should Be Greater Than First Requested Date';
				}
			
    return $result;
}, 10, 4 );

/* Carpet Cleaning Form */
add_filter( 'gform_field_validation_15_17', function ( $result, $value, $form, $field ) {
        $dateOne = rgpost( 'input_13' );
        $dateTwo = rgpost( 'input_16' );
        $timeOne = rgpost( 'input_12' );
        if($dateOne == $dateTwo){
            if ( $result['is_valid'] && $value == $timeOne ) {
                $result['is_valid'] = false;
                $result['message']  = 'Please enter a different Time.';
            }
        }
    return $result;
}, 10, 4 );

add_filter( 'gform_field_validation_15_16', function ( $result, $value, $form, $field ) {
	$dateOne = rgpost( 'input_13' );       
	if ( $result['is_valid'] && $value < $dateOne ) {
		$result['is_valid'] = false;
		$result['message']  = 'Second Requested Date Should Be Greater Than First Requested Date';
	}
    return $result;
}, 10, 4 );

/* Get a Free Estimate Form */
add_filter( 'gform_field_validation_36_15', function ( $result, $value, $form, $field ) {
        $dateOne = rgpost( 'input_7' );
        $dateTwo = rgpost( 'input_14' );
        $timeOne = rgpost( 'input_8' );
        if($dateOne == $dateTwo){
            if ( $result['is_valid'] && $value == $timeOne ) {
                $result['is_valid'] = false;
                $result['message']  = 'Please enter a different Time.';
            }
        }
    return $result;
}, 10, 4 );

add_filter( 'gform_field_validation_36_14', function ( $result, $value, $form, $field ) {
	$dateOne = rgpost( 'input_7' );       
	if ( $result['is_valid'] && $value < $dateOne ) {
		$result['is_valid'] = false;
		$result['message']  = 'Second Requested Date Should Be Greater Than First Requested Date';
	}
    return $result;
}, 10, 4 );

/* Gravity Form ajax submission flase  */

add_filter('gform_form_args', 'no_ajax_on_all_forms', 10, 1);
function no_ajax_on_all_forms($args){
    $args['ajax'] = false;
    return $args;
}

//add method to register event to WordPress init
add_action( 'init', 'register_daily_mysql_bin_log_event');
 
function register_daily_mysql_bin_log_event() {
    // make sure this event is not scheduled
    if( !wp_next_scheduled( 'mysql_bin_log_job' ) ) {
        // schedule an event
        wp_schedule_event( time(), 'daily', 'mysql_bin_log_job' );
    }
}

add_action( 'mysql_bin_log_job', 'mysql_bin_log_job_function' );
function mysql_bin_log_job_function() {
   
    global $wpdb;
    $yesterday = date('Y-m-d',strtotime("-1 days"));
    $sql_delete = "PURGE BINARY LOGS BEFORE '$yesterday'" ;						
	$delete_endpoint = $wpdb->get_results($sql_delete);
   
}
add_filter( 'auto_update_plugin', '__return_false' );